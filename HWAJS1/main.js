class Employee {
    constructor (name, age, salary) {
        this._name = name
        this._age = age
        this._salary = salary
    }
    get FullEmployee(){
        return this._name; 
        return this._age;
        return this._salary
    }
    
    set FullEmployeeSet(value){
        this._name = value;
        this._age = value;
        this._salary = value;

    }
}

const user1 = new Employee ("Ivan", 32, 500);
console.log (user1);
console.log(user1.FullEmployee);


class Programmer extends Employee {
    constructor (name, age, salary, lang) {
        super (name, age, salary)
        this.lang = lang
    }
    get programmerSalary() {
        return this._salary * 3
    }
}
const user2 = new Programmer ("Mark", 22, 700);
const user3 = new Programmer ("Iruna", 28, 600);
console.log(user2);
console.log(user3);
console.log(user2.programmerSalary);
console.log(user3.programmerSalary);
console.log(`${user2._name} has a new salary ${user2.programmerSalary}`);
console.log(`${user3._name} has a new salary ${user3.programmerSalary}`);
